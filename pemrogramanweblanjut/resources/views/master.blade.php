<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>Membuat 3 Halaman</title>
    <style>
        nav {
        list-style: none;
        margin: 0;
        padding: 0;
        }
        nav li {
        float: left;
        }
        nav a {
        text-decoration: none;
        color: black;
        padding: 10px 20px;
        height: 20px;
        display: block;
        background-color: #C2C2C2;
        }
        nav a:hover {
        color: white;
        background-color: #838181;
        }
        </style>
</head>
<body>

	<header>

		<h2>Welcome</h2>
		<nav>
            <li><a href="/blog">HOME</a></li>
            <li><a href="/blog/tentang">TENTANG</a></li>
			<li><a href="/blog/kontak">KONTAK</a></li>

		</nav>
	</header>
	<hr/>
	<br/>
	<br/>

	<h3> @yield('judul_halaman') </h3>
	@yield('konten')


	<br/>
	<br/>
	<hr/>


</body>
</html>
